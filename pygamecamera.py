import time
import pygame, sys
import pygame.camera
from pygame.locals import *
import RPi.GPIO as GPIO




class Pygamecamera(object):
   
    
    def __init__(self):
       # self.frames = [open(f + '.jpg', 'rb').read() for f in ['1', '2', '3']]

        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(20,GPIO.OUT)
        GPIO.setup(21,GPIO.OUT)
        GPIO.output(21,True)


        pygame.init()
        pygame.camera.init()

        self.cam = pygame.camera.Camera("/dev/video0",(320,240),'HSV')
   
        time.sleep(1)
          
        self.cam.start()

        

    def get_frame(self):
        GPIO.output(20,not GPIO.input(20))
        img = self.cam.get_image()
        pygame.image.save(img,'image.jpg')
        self.frame = open('image.jpg','rb').read()
        
        return self.frame
