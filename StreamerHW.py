#http://blog.miguelgrinberg.com/post/video-streaming-with-flask
#!/usr/bin/env python
from flask import Flask, render_template, Response
from pygamecamera import Pygamecamera
import time

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('StreamerHW.html')

def gen(pygamecamera):
    while True:
        time.sleep(1)
        
        frame = pygamecamera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

@app.route('/video_feed')
def video_feed():
    return Response(gen(Pygamecamera()),mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    app.run(host='192.168.0.128', port=8080,debug=True)
